package org.tcl.composite;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExpressionTest {

    @Test
    public void evaluate_constant() {
        Expression e = new Constant(3);
        assertEquals(3, e.evaluate(), 0.0);
    }

    @Test
    public void evaluate_adder() {
        Expression e = new Adder(new Constant(3), new Constant(5));
        assertEquals(8, e.evaluate(), 0.0);
    }

    @Test
    public void evaluate_subtracter() {
        Expression e = new Subtracter(new Constant(3), new Constant(5));
        assertEquals(-2, e.evaluate(), 0.0);
    }

    @Test
    public void evaluate_multiplier() {
        Expression e = new Multiplier(new Constant(3), new Constant(5));
        assertEquals(15, e.evaluate(), 0.0);
    }

    @Test
    public void evaluate_divider() {
        Expression e = new Divider(new Constant(15), new Constant(5));
        assertEquals(3, e.evaluate(), 0.0);
    }

    @Test
    public void evaluate_complex() {

        //((100/10)+20)/(2.5*4)
        Expression e = new Divider(new Adder(new Divider(new Constant(100), new Constant(10)), new Constant(20)), new
                Multiplier(new Constant(2.5), new Constant(4)));
        assertEquals(3, e.evaluate(), 0.0);
    }

}