package org.tcl.composite;

public class Multiplier extends BinaryExpression {

    public Multiplier(Expression lhs, Expression rhs) {
        super(lhs, rhs);
    }

    public double evaluate() {
        return lhs.evaluate() * rhs.evaluate();
    }
}
