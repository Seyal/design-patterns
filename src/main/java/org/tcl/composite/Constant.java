package org.tcl.composite;

public class Constant implements Expression {
    private double value;

    public Constant(double i) {
        this.value = i;
    }

    public double evaluate() {
        return value;
    }
}
