package org.tcl.composite;

public abstract class BinaryExpression implements
        Expression {
    protected Expression lhs;
    protected Expression rhs;

    public BinaryExpression(Expression lhs, Expression
            rhs) {
        this.rhs = rhs;
        this.lhs = lhs;
    }

    public abstract double evaluate();

}
