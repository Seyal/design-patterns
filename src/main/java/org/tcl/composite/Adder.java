package org.tcl.composite;

public class Adder extends BinaryExpression {

    public Adder(Expression lhs, Expression rhs) {
        super(lhs, rhs);
    }

    public double evaluate() {
        return lhs.evaluate() + rhs.evaluate();
    }
}
