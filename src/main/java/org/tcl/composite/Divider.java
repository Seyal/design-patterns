package org.tcl.composite;

public class Divider extends BinaryExpression {

    public Divider(Expression lhs, Expression rhs) {
        super(lhs, rhs);
    }

    public double evaluate() {
        return lhs.evaluate() / rhs.evaluate();
    }
}
