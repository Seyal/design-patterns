package org.tcl.composite;

public class Subtracter extends BinaryExpression {

    public Subtracter(Expression lhs, Expression rhs) {
        super(lhs, rhs);
    }

    public double evaluate() {
        return this.lhs.evaluate() - this.rhs.evaluate();
    }

}
