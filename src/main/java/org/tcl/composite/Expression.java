package org.tcl.composite;

public interface Expression {

    double evaluate();
}
